import re
import os


class Lifecycle():
    def __init__(self, name, timeframe):
        self.name = name
        self.timeframe = timeframe

    def __str__(self):
        return "name: {0}, time frame: {1}".format(self.name, self.timeframe)


class Activity():
    def __init__(self, name, lcs):
        self.name = name
        self.lcs = lcs

    def __str__(self):
        return "name: {0}, lifecycle stage: [{1}]".format(self.name, self.lcs.__str__())


class Channel():
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "name: {0}".format(self.name)


class Touchpoint():
    def __init__(self, name, channel):
        self.name = name
        self.channel = channel

    def __str__(self):
        return "name: {0}, channel: {1}".format(self.name, self.channel.name)


class Interaction():  # node, page
    def __init__(self, name, touchpoint, activity):
        self.name = name
        self.touchpoint = touchpoint
        self.activity = activity

    def __str__(self):
        return "name: {0}, touchpoint: [{1}], activity: [{2}]".format(self.name, self.touchpoint.__str__(),
                                                                      self.activity.__str__())

    def key(self, touchpoint=False, activity=False):
        if touchpoint and activity:
            return "{0}:{1}:{2}:{3}".format(self.activity.lcs.name, self.touchpoint.channel.name, self.touchpoint.name,
                                            self.activity.name)
        elif touchpoint:
            return "{0}::{1}".format(self.activity.lcs.name, self.touchpoint.name)
        elif activity:
            return "{0}:{1}::{2}".format(self.activity.lcs.name, self.touchpoint.channel.name, self.activity.name)
        else:
            return "{0}:{1}".format(self.activity.lcs.name, self.touchpoint.channel.name)


class Flow():
    def __init__(self, page_from, page_to, time_delta, volume, helped):
        self.page_from = page_from
        self.page_to = page_to
        self.time_delta = time_delta
        self.volume = volume
        self.helped = helped

    def __str__(self):
        return "from: [{0}], to: [{1}], delta: {2}, volume {3}, helped: {4}".format(self.page_from, self.page_to,
                                                                                    self.time_delta, self.volume,
                                                                                    self.helped)


# table pattern
# TABLE = r'\s+(\|[\w\s\|\.:/,]+\|[\r\n])+\s+Scenario:'
TABLE = r'\s((\|[\w \t\|\.:/,]+\|[\r\n])+)[\r\n]{1,2}Scenario:'

# steps
GIVEN_LIFECYCLES = r'Given REST lifecycle stages:'
GIVEN_ACTIVITIES = r'Given REST activity types:'
GIVEN_TOUCHPOINTS = r'Given REST touchpoints:'
GIVEN_INTERACTIONS = r'Given REST interactions:'
GIVEN_JOURNEYS = r'Given the journeys:'

LIFECYCLES = "Lifecycles"
ACTIVITIES = "Activities"
CHANNELS = "Channels"
TOUCHPOINTS = "Touchpoints"
INTERACTIONS = "Interactions"
FLOWS = "Flows"

# columns
LCS = 'lifecycle stage'
TIME_FRAME = 'time frame'
TIME_UNIT = 'time frame unit'  # it's ignored
ACTIVITY = 'activity type'
TOUCHPOINT = 'touchpoint'
CHANNEL = 'channel'
INTERACTION = 'interaction'
FLOW = 'flow'
TIME_DELTA = 'time delta'
VOLUME = 'volume'


def save_lifecycles(table, context):
    if not LIFECYCLES in context:
        context[LIFECYCLES] = {}
    col_positions = get_columns_positions(table)
    values = get_column_values(table, col_positions)
    for row in values:
        lcs = Lifecycle(row[LCS], int(row[TIME_FRAME]))
        context[LIFECYCLES][lcs.name] = lcs
        print("Added lifecycle stage: [{0}]".format(lcs.__str__()))


def save_activities(table, context):
    if not ACTIVITIES in context:
        context[ACTIVITIES] = {}
    col_positions = get_columns_positions(table)
    values = get_column_values(table, col_positions)
    for row in values:
        activity = Activity(row[ACTIVITY], context[LIFECYCLES][row[LCS]])
        context[ACTIVITIES][activity.name] = activity
        print("Added activity type: [{0}]".format(activity.__str__()))


def save_touchpoints(table, context):
    if not TOUCHPOINTS in context:
        context[TOUCHPOINTS] = {}
    if not CHANNELS in context:
        context[CHANNELS] = {}
    col_positions = get_columns_positions(table)
    values = get_column_values(table, col_positions)
    for row in values:
        if row[CHANNEL].upper() in context[CHANNELS]:
            channel = context[CHANNELS][row[CHANNEL].upper()]
        else:
            channel = Channel(row[CHANNEL].upper())
            context[CHANNELS][channel.name] = channel
        touchpoint = Touchpoint(row[TOUCHPOINT], channel)
        context[TOUCHPOINTS][touchpoint.name] = touchpoint
        print("Added touchpoint: [{0}]".format(touchpoint.__str__()))


def save_interactions(table, context):
    if not INTERACTIONS in context:
        context[INTERACTIONS] = {}
    col_positions = get_columns_positions(table)
    values = get_column_values(table, col_positions)
    for row in values:
        if ACTIVITY in row:
            touchpoint = context[TOUCHPOINTS][row[TOUCHPOINT]]
            activity = context[ACTIVITIES][row[ACTIVITY]]
            interaction = Interaction(row[INTERACTION], touchpoint, activity)
            context[INTERACTIONS][interaction.name] = interaction
            print("Added interaction: [{0}]".format(interaction.__str__()))


def save_flows(table, context):
    if not FLOWS in context:
        context[FLOWS] = []
    col_positions = get_columns_positions(table)
    values = get_column_values(table, col_positions)
    for row in values:
        pages = [f.strip() for f in row[FLOW].split(',')]
        deltas = [int(d.strip()) for d in row[TIME_DELTA].split(',')]
        volume = int(row[VOLUME])
        page_from = context[INTERACTIONS][pages[0]]
        page_to = get_page_to(context, pages)
        helped = len(pages) == 3
        flow = Flow(page_from, page_to, max(deltas), volume, helped)
        context[FLOWS].append(flow)
        print("Added flow: [{0}]".format(flow))


def get_page_to(context, pages):
    if len(pages) == 1:
        return None
    elif len(pages) == 2:
        return context[INTERACTIONS][pages[1]]
    elif len(pages) == 3:
        return context[INTERACTIONS][pages[2]]
    else:
        raise Exception("Unsupported flow: {0}".format(pages))


def split_row(row):
    cols = row.split('|')
    return [cols[i].strip() for i in range(1, len(cols) - 1)]  # 2 empty string - leftmost and rightmost


def get_column_values(table, cols_positions):
    """Returns all splitted rows from the table except the header"""
    print("----------------------------------------------------------------------------------------------------------")
    print(table)
    rows = re.findall(r'^\|[\w \t\|\.:/,]+\|$', table, re.MULTILINE)
    values = []
    # print(cols_positions)
    for i in range(1, len(rows)):
        cols = split_row(rows[i])
        # print(cols)
        row = {}
        for header in cols_positions.keys():
            row[header] = cols[cols_positions[header]]
        values.append(row)
    return values


def get_columns_positions(table):
    header = re.search(r'(.*)[\r\n]', table).group(1)
    cols = split_row(header)
    positions = {}
    for i in range(len(cols)):
        positions[cols[i]] = i
    return positions


def process_tables(step_pattern, func, story, context):
    tables = re.findall(step_pattern + TABLE, story)
    if tables:
        for table in tables:
            func(table[0], context)


def get_context(story_path):
    context = {}
    with open(story_path, 'r') as journeys:
        story = journeys.read()
        print(story)
        process_tables(GIVEN_LIFECYCLES, save_lifecycles, story, context)
        process_tables(GIVEN_ACTIVITIES, save_activities, story, context)
        process_tables(GIVEN_TOUCHPOINTS, save_touchpoints, story, context)
        process_tables(GIVEN_INTERACTIONS, save_interactions, story, context)
        process_tables(GIVEN_JOURNEYS, save_flows, story, context)
    return context


# metrics
DROP_INS = "drop ins"
DROP_OFFS = "drop offs"
ACTIVE = "active"
ACTIVE_HELPED = "active helped"
INACTIVE = "inactive"
INACTIVE_HELPED = "inactive helped"
ON_STEP = "on step"
FROM = "from"
TO = "to"


def volumes(context, key, touchpoint=False, activity=False):
    print(key)
    vols = {DROP_INS: 0,
            DROP_OFFS: 0,
            ACTIVE: 0,
            ACTIVE_HELPED: 0,
            INACTIVE: 0,
            INACTIVE_HELPED: 0,
            ON_STEP: 0,
            FROM: 0,
            TO: 0}
    for flow in context[FLOWS]:
        if flow.page_from.key(touchpoint, activity) == key:
            if flow.time_delta <= flow.page_from.activity.lcs.timeframe:
                vols[DROP_INS] += flow.volume
                if flow.time_delta <= 0.8 * flow.page_from.activity.lcs.timeframe:
                    vols[ACTIVE] += flow.volume
                else:
                    vols[INACTIVE] += flow.volume
                if flow.page_to:
                    if flow.page_to.key(touchpoint, activity) != flow.page_from.key(touchpoint, activity):
                        vols[FROM] += flow.volume
                        if flow.time_delta <= 0.8 * flow.page_to.activity.lcs.timeframe:
                            vols[ACTIVE] -= flow.volume
                        else:
                            vols[INACTIVE] -= flow.volume
        if flow.page_to and flow.page_to.key(touchpoint, activity) == key:
            if flow.time_delta <= flow.page_to.activity.lcs.timeframe:
                if flow.page_to.key(touchpoint, activity) != flow.page_from.key(touchpoint, activity):
                    vols[TO] += flow.volume
                    if flow.time_delta <= 0.8 * flow.page_to.activity.lcs.timeframe:
                        if not flow.helped:
                            vols[ACTIVE] += flow.volume
                        else:
                            vols[ACTIVE_HELPED] += flow.volume
                    else:
                        if not flow.helped:
                            vols[INACTIVE] += flow.volume
                        else:
                            vols[INACTIVE_HELPED] += flow.volume
            if flow.time_delta == flow.page_from.activity.lcs.timeframe:
                vols[DROP_OFFS] += flow.volume
                if not flow.helped:
                    vols[INACTIVE] -= flow.volume
                else:
                    vols[INACTIVE_HELPED] -= flow.volume
    vols[ON_STEP] = vols[DROP_INS] - vols[FROM] + vols[TO] - vols[DROP_OFFS]
    vols[ACTIVE] += vols[ACTIVE_HELPED]
    vols[INACTIVE] += vols[INACTIVE_HELPED]
    return vols


def transition(from_key, to_key, touchpoint_from=False, touchpoint_to=False, activity_from=False, activity_to=False):
    transitions = 0
    for flow in context[FLOWS]:
        if from_key is None:    # drop in
            if flow.page_from.key(touchpoint_from, activity_from) == to_key:
                transitions += flow.volume
        elif to_key is None:    # drop off
            if flow.page_to \
                    and flow.page_to.key(touchpoint_to, activity_to) == from_key \
                    and flow.time_delta >= flow.page_to.activity.lcs.timeframe:
                transitions += flow.volume
        elif flow.page_to \
                and flow.page_from.key(touchpoint_from, activity_from) == from_key \
                and flow.page_to.key(touchpoint_to, activity_to) == to_key:
            transitions += flow.volume
    return transitions


STORIES_PATH = r'C:\th\thx\repo\juice-test\juice-test-behaviour\src\main\stories'
FILE = r'individuals\analytics\journeys\journeys_touchpoint_view.story'

if __name__ == "__main__":
    context = get_context(os.path.join(STORIES_PATH, FILE))
    # print(volumes(context, "Awareness:WEB"))
    for key in set([i.key(touchpoint=True) for i in context[INTERACTIONS].values()]):
        print(volumes(context, key, touchpoint=True))
    print(transition("Activation::assistedTouch_05", "Service:SOCIAL", touchpoint_from=True))