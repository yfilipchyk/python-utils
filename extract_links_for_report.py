
import re
import os.path as op

DIR = "C:\\th\\thx\\temp"
SOURCE_NAME = "bamboo_tesults.txt"  # insert build results into this file
CSV_NAME = "bamboo_tesults.csv"     # resulting report

BUILD_NUMBER = "THX-AUTOMATION1-12" # change this value for each build
BUILD_URL = "https://thunderhead.jira.com/builds/browse/" + BUILD_NUMBER
PATH_TO_VIEW = BUILD_URL + "/artifact/JOB1/JBehave-report/view/"

file = open(op.join(DIR, SOURCE_NAME), "r")
csv = open(op.join(DIR, CSV_NAME), "w")

csv.write('"Story","View url","Failed step"\n')

stories_found = 0
csv_line = ""
for line in file.readlines():
    m = re.search('JBehave (.*)\.story History', line)
    if m is not None:
        story = m.group(1) + ".story"
        view_url = PATH_TO_VIEW + re.sub('/', '.', m.group(1)) + ".html"
        csv_line = '"' + story + '","' + view_url + '",'
        stories_found += 1
    else:
        m = re.search('Step: (.*)\n', line)
        if m is not None:
            csv_line += '"' + m.group(1) + '"\n'
            csv.write(csv_line)
    
print("STORIES FOUND:", stories_found)
file.close()
csv.close()